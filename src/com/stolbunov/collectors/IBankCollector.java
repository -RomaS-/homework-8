package com.stolbunov.collectors;

import java.util.Map;

public interface IBankCollector {
    Map<Integer, Integer> getMapDebtors();
}
