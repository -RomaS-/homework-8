package com.stolbunov.collectors;

import com.stolbunov.ICollector;
import com.stolbunov.IPeople;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Collector implements ICollector {
    private Map<Integer, Integer> debtors;

    public Collector() {
        debtors = new HashMap<>();
    }

    @Override
    public void takeMapDebtors(Map<Integer, Integer> mapDebtors) {
        if (debtors.isEmpty()) {
            debtors.putAll(mapDebtors);
        } else {
            addToDebt(mapDebtors);
        }
    }

    private void addToDebt(Map<Integer, Integer> mapDebtors) {
        Set<Integer> passportData = mapDebtors.keySet();
        for (Integer pas : passportData) {
            if (debtors.containsKey(pas)) {
                debtors.put(pas, debtors.get(pas) + mapDebtors.get(pas));
            } else {
                debtors.put(pas, mapDebtors.get(pas));
            }

        }
    }

    @Override
    public void printTotalDebtClient(IPeople client) {
        System.out.println("\tThe total debt of the customer " +
                "<<" + client.getName() + ">> is " + debtors.get(client.getPassportData()) + "$");
    }


}
