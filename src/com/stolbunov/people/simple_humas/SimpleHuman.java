package com.stolbunov.people.simple_humas;

import com.stolbunov.people.BasePeople;
import com.stolbunov.people.IBankClient;

import java.util.HashMap;
import java.util.Map;

public class SimpleHuman extends BasePeople {
    private Map<String, Integer> listCredits;

    SimpleHuman(String name) {
        super(name);
        listCredits = new HashMap<>();
    }

    @Override
    public void takeCredit(String nameBank, int credit) {
        listCredits.put(nameBank, credit);
    }

    @Override
    public void goToBank(IBankClient bank) {
        if (dropTheCoin()) {
            bank.giveCredit(this);
        }
    }
}
