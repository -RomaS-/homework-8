package com.stolbunov;

import com.stolbunov.people.IBankClient;

public interface IPeople {
    void goToBank(IBankClient bank);

    int getPassportData();

    String getName();
}
