package com.stolbunov;

import com.stolbunov.banks_system.banks.BankFactory;
import com.stolbunov.banks_system.banks.IClient;
import com.stolbunov.banks_system.computer_banks.Computer;
import com.stolbunov.collectors.Collector;
import com.stolbunov.people.IBankClient;
import com.stolbunov.people.simple_humas.SimpleHumanFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {
    private static final int AMOUNT_PEOPLE = 10;
    private static final int AMOUNT_BANK = 3;
    private static final int GONE_TIME = 4;
    private static List<IPeople> people;
    private static List<IBank> banks;
    private static ICollector collector;
    private static IComputerTime time;

    public static void main(String[] args) {
        collector = new Collector();
        time = new Computer();

        people = createClients();
        banks = createBank();
        clientsGoToBanks();
        printMapDebtors("in the beginning");
        counterYears();
        purchaseMapDebtors();
        printMapDebtors(" redeemed by Collector after " + GONE_TIME + " years");
        printTotalDebtClient();

    }

    private static void purchaseMapDebtors() {
        for (IBank bank : banks) {
            Map<Integer, Integer> mapDebtors = bank.getMapDebtors();
            collector.takeMapDebtors(mapDebtors);
        }
    }

    private static void counterYears() {
        time.setGoneTime(GONE_TIME);
    }

    private static void clientsGoToBanks() {
        for (IPeople people : people) {
            for (IBankClient bank : banks) {
                people.goToBank(bank);
            }

            for (IBankClient bank : banks) {
                people.goToBank(bank);
            }
        }
    }

    private static List<IPeople> createClients() {
        List<IPeople> clients = new ArrayList<>();
        IPeopleFactory peopleFactory = new SimpleHumanFactory();
        for (int i = 0; i < AMOUNT_PEOPLE; i++) {
            clients.add(peopleFactory.create());
        }
        return clients;
    }

    private static List<IBank> createBank() {
        List<IBank> banks = new ArrayList<>(AMOUNT_BANK);
        IBankFactory factory = new BankFactory();
        banks.add(factory.createRedBank());
        banks.add(factory.createYellowBank());
        banks.add(factory.createGreenBank());
        return banks;
    }

    private static void printMapDebtors(String text) {
        System.out.println("\nList of debtors" + text + ":");
        for (IBank bank : banks) {
            bank.printMapDebtors();
        }
    }

    private static void printTotalDebtClient() {
        System.out.println("<<Collector>>");
        for (IPeople client : people) {
            collector.printTotalDebtClient(client);
        }
    }
}
