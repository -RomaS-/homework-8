package com.stolbunov.banks_system.banks;

import com.stolbunov.IBank;
import com.stolbunov.banks_system.computer_banks.Computer;
import com.stolbunov.banks_system.computer_banks.IBankComputer;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseBank implements IBank, IBankComputer {
    protected IComputer computer;
    private Map<Integer, String> mapNameClients;

    public BaseBank() {
        mapNameClients = new HashMap<>();
        computer = new Computer();
    }

    protected void printMap(IBankComputer bank) {
        Map<Integer, Integer> allDebtors = computer.getAllDebtors(bank);
        System.out.println("\t" + bank.getNameBank());
        allDebtors.forEach((integer, integer2) ->
                System.out.println("\t\tThe passport number of the client <<" + mapNameClients.get(integer) + ">>  his debt <<" + integer2 + ">>"));
    }

    @Override
    public void giveCredit(IClient client) {
        if (!mapNameClients.containsKey(client.getPassportData())) {
            mapNameClients.put(client.getPassportData(), client.getName());
        }
    }
}
