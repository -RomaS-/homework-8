package com.stolbunov.banks_system.banks;

import java.util.Map;

public interface IDatabase {
    void addDatabase(String nameBank, int passportData, int sumCredit);

    boolean isClientDB(String nameBank, int passportData);

    Map<Integer, Integer> getAllClientWithDebts(String nameBank);
}
