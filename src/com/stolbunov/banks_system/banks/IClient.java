package com.stolbunov.banks_system.banks;

public interface IClient {
    void takeCredit(String nameBank, int credit);

    int getPassportData();

    String getName();
}
