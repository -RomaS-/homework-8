package com.stolbunov.banks_system.database_of_banks.exception.not_found;

import com.stolbunov.banks_system.database_of_banks.exception.DBException;

public class NotFoundException extends DBException {
    public NotFoundException() {
        super();
    }

    public NotFoundException(String message) {
        super(message);
    }
}
