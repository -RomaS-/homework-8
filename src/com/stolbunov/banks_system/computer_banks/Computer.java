package com.stolbunov.banks_system.computer_banks;

import com.stolbunov.IComputerTime;
import com.stolbunov.banks_system.banks.IClient;
import com.stolbunov.banks_system.banks.IComputer;
import com.stolbunov.banks_system.banks.IDatabase;
import com.stolbunov.banks_system.database_of_banks.Database;

import java.util.Map;
import java.util.Set;

public class Computer implements IComputer, IComputerTime {
    private static int goneTime;
    private IDatabase database;

    public Computer() {
        database = Database.getInstance();
    }

    @Override
    public boolean processAddClient(IBankComputer bank, int sumCredit, IClient client) {
        int passportData = client.getPassportData();
        String nameBank = bank.getNameBank();
        if (!database.isClientDB(nameBank, passportData)) {
            messageSuccessfulOperation(sumCredit, client, nameBank);
            database.addDatabase(nameBank, passportData, sumCredit);
            return true;
        } else {
            messageDenialOfCredit(sumCredit, client, nameBank);
            return false;
        }
    }

    @Override
    public Map<Integer, Integer> getAllDebtors(IBankComputer bank) {
        return getAllDebtors(bank, 0);
    }

    @Override
    public Map<Integer, Integer> getAllDebtors(IBankComputer bank, float interestRate) {
        Map<Integer, Integer> map = database.getAllClientWithDebts(bank.getNameBank());
        if (goneTime != 0) {
            return considerDebts(map, interestRate);
        }
        return map;
    }

    private Map<Integer, Integer> considerDebts(Map<Integer, Integer> map, float interestRate) {
        Set<Integer> clients = map.keySet();
        for (Integer client : clients) {
            int newDebt = map.get(client);
            int percentGoneTime = (int) (newDebt * interestRate) * goneTime;
            newDebt += percentGoneTime;
            map.put(client, newDebt);
        }
        return map;
    }

    @Override
    public void setGoneTime(int goneTime) {
        Computer.goneTime = goneTime;
        System.out.println("\n<<Computer>> After " + goneTime + " years");
    }

    private void messageSuccessfulOperation(int sumCredit, IClient client, String nameBank) {
        System.out.println("<<" + nameBank + ">> \n\tThe operation of issuing the loan was completed successfully!" +
                "\n\tClient passport data <<" + client.getName() + ">> " +
                "got credit for the amount of <<" + sumCredit + ">>\n");
    }

    private void messageDenialOfCredit(int sumCredit, IClient client, String nameBank) {
        System.out.println("<<" + nameBank + ">> \n\tSorry but you are denied credit! " +
                "\n\tYou already took out a loan from us: " +
                "\n\t\tClient passport data <<" + client.getName() + ">> amount of credit <<" + sumCredit + ">>\n");
    }
}
