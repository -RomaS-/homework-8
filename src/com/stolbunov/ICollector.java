package com.stolbunov;

import java.util.Map;

public interface ICollector {

    void takeMapDebtors(Map<Integer, Integer> mapDebtors);

    void printTotalDebtClient(IPeople client);
}
